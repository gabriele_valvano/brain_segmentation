<b>Brain Segmentation.</b>

Author: <b>Gabriele Valvano</b>

___________________________
This project contains Tensorflow code for skull-stripping neural net. 

There are two implementations: for networks working on 2D slices or 3D volume (respectively: UNet, under the unet_2d folder, and VNet, under the unet_3d folder). Apart from the input dimension and the way in which the test is performed (in order to take into account all the volumetric information in both 2D and 3D cases), the architectures differ in the use of 2D or 3D convolutional kernels.

Results in Table 1 show the comparison between the learning algorithms and the most popular classical tools. Figure 1, instead, illustrates an example of brain segmentation operated by VNet.

![table](results/images/table1.png)

![image](results/images/image1.png)

___________________________
The code has been developed in the course of research activities and it has to be intended as provisional and/or continuously updated.

To use of this code, in addition to the most common Python libraries, the installation of Tensorflow is required.

If you intend to use this code, please cite me :) 

For any errors, typos or code improvement, please send me a message. You can find my email address in one of my pubblications.
You can click <a href="https://gitlab.com/gabriele_valvano">here</a> to get to my profile on GitLab.