"""
Configuration file for unet 2d
"""
#  Copyright 2019 Gabriele Valvano
# 
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import tensorflow as tf
import os

RUN_ID = 'brain_segm_unet2d'

volume_data_root = 'data/NFBS_Dataset'

# ________________________________________________ #


def get_volume_path(root):
    # returns list of NFBS dataset brain file under root
    list_path = []
    for dirName, subdirList, fileList in os.walk(root):
        subdirList.sort()
        for subdir in subdirList:
            if not subdir.startswith('.'):
                list_path.append(root + os.sep + subdir + os.sep + 'sub-' + subdir + '_ses-NFB3_T1w.nii.gz')
    return list_path


list_of_files_train = get_volume_path(volume_data_root + os.sep + 'train_set')
list_of_files_valid = get_volume_path(volume_data_root + os.sep + 'validation_set')

# ________________________________________________ #


def define_flags():
    FLAGS = tf.flags.FLAGS
    tf.flags.DEFINE_string('RUN_ID', RUN_ID, "")

    # ____________________________________________________ #
    # ========== ARCHITECTURE HYPER-PARAMETERS ========== #

    # Learning rate:
    lr = 1e-3
    tf.flags.DEFINE_float('lr', lr, "learning rate")

    # batch size
    tf.flags.DEFINE_integer('b_size', 1, "batch size")

    # ____________________________________________________ #
    # =============== TRAINING STRATEGY ================== #

    # _________________________________
    # Learning rate annealing strategy:
    tf.flags.DEFINE_bool('perform_lr_annealing', False, "whether to perform annealing of the learning rate")
    strategies = ('step_decay', 'exp_decay', '1overT_decay')
    # ----
    decay_factor = 2
    decay_period = 300  # used for step decay annealing
    strategy = strategies[0]  # annealing strategy
    delay = 600  # epochs before annealing the first time
    # ----
    string_dict = "{" + "'lr0': {0}, 'k': {1}, 'period': {2}".format(lr, decay_factor, decay_period) + "}"
    tf.flags.DEFINE_string('annealing_strategy', strategy, "annealing strategy")
    tf.flags.DEFINE_integer('annealing_epoch_delay', delay, "number of epoch to wait before using the annealing strategy")
    tf.flags.DEFINE_string('annealing_parameters', string_dict, "parameters to be used by the annealing strategy")

    # __________________________________
    # DSD (Dense-Sparse-Dense) training:
    tf.flags.DEFINE_bool('perform_sparse_training', False, "whether to perform the sparse training")
    tf.flags.DEFINE_float('sparsity', 0.30, "sparsity constraint for DSD training")
    tf.flags.DEFINE_float('beta', 0.10, "multiplying factor for the learning rate")

    # ____________________________________________________ #
    # =============== INTERNAL VARIABLES ================= #

    # internal variables:
    tf.flags.DEFINE_integer('num_threads', 11, "number of threads for loading data")
    tf.flags.DEFINE_integer('skip_step', 20, "frequency of printing batch report")
    tf.flags.DEFINE_bool('tensorboard_verbose', True, "if True: save also layers weights every N epochs")

    # ____________________________________________________ #
    # ===================== DATASET ====================== #

    # path data:
    tf.flags.DEFINE_list('list_of_files_train', list_of_files_train, """List of file for train.""")
    tf.flags.DEFINE_list('list_of_files_valid', list_of_files_valid, """List of file for validation.""")

    return FLAGS
