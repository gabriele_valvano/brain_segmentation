"""
Interface with the dataset for unet 2d
"""
#  Copyright 2019 Gabriele Valvano
# 
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
import nibabel as nib
import numpy as np
from idas.data_augmentation.transformations import *
from idas.utils import get_available_gpus
import scipy.ndimage
from tensorflow.contrib.data.python.ops import prefetching_ops
from skimage.measure import block_reduce
from idas.data_augmentation.data_augm_3d_pyops import add_linear_bias
from idas.data_augmentation.numpy_utils import np_zero_pad


class DatasetInterface(object):
    @staticmethod
    def one_hot_encode(y, nb_classes):
        # TODO: metodo da rimuovere; lasciare invece l'operazione tf.one_hot all'interno del grafo
        y_shape = list(y.shape)
        y_shape.append(nb_classes)
        with tf.device('/cpu:0'):
            with tf.Session() as sess:
                res = sess.run(tf.one_hot(indices=y, depth=nb_classes))
        return res.reshape(y_shape)

    def _data_augmentation_ops(self, x_batch, y_batch):

        n_samples, dim0, dim1, dim2 = x_batch.shape
        coords = np.meshgrid(np.arange(dim0), np.arange(dim1), np.arange(dim2))

        # stack the meshgrid to position vectors, center them around 0 by substracting dim/2
        xyz = np.vstack([coords[0].reshape(-1) - float(dim0) / 2,   # x coordinate, centered
                         coords[1].reshape(-1) - float(dim1) / 2,   # y coordinate, centered
                         coords[2].reshape(-1) - float(dim2) / 2,   # z coordinate, centered
                         np.ones((dim0, dim1, dim2)).reshape(-1)])  # 1 for homogeneous coordinates

        origin, xaxis, yaxis, zaxis = [0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]

        # create and apply transformation matrix for each element + add noise in the end
        for i in range(n_samples):
            # sample transformation parameters for the current element of the batch
            scale_factor = np.random.uniform(low=0.95, high=1.05)
            trasl_factor = np.random.uniform(low=-5, high=5, size=3)
            shear_factor = np.random.uniform(low=0, high=np.pi / 45)  # 0°, +4°
            alpha = np.random.uniform(low=-np.pi / 18, high=np.pi / 18)  # -10°, +10°
            beta = np.random.uniform(low=-np.pi / 18, high=np.pi / 18)  # -10°, +10°
            gamma = np.random.uniform(low=-np.pi / 18, high=np.pi / 18)  # -10°, +10°

            S = scale_matrix(scale_factor, origin)
            T = translation_matrix(trasl_factor)
            Z = shear_matrix(shear_factor, xaxis, origin, yaxis)

            R = concatenate_matrices(
                rotation_matrix(alpha, xaxis),
                rotation_matrix(beta, yaxis),
                rotation_matrix(gamma, zaxis))

            refl_axis = np.random.permutation(np.arange(6))
            refl = [reflection_matrix(origin, xaxis),
                    reflection_matrix(origin, yaxis),
                    reflection_matrix(origin, zaxis),
                    identity_matrix(),
                    identity_matrix(),
                    identity_matrix()]
            Refl = concatenate_matrices(refl[refl_axis[0]], refl[refl_axis[1]], refl[refl_axis[2]])

            M = concatenate_matrices(Refl, T, R, Z, S)

            # apply transformation
            transformed_xyz = np.matmul(M, xyz)

            # extract coordinates, don't use transformed_xyz[3,:] that's the homogeneous coordinate, always 1
            x = transformed_xyz[0, :] + float(dim0) / 2
            y = transformed_xyz[1, :] + float(dim1) / 2
            z = transformed_xyz[2, :] + float(dim2) / 2

            x = x.reshape((dim0, dim1, dim2))
            y = y.reshape((dim0, dim1, dim2))
            z = z.reshape((dim0, dim1, dim2))

            # the coordinate system seems to be strange, it has to be ordered like this
            new_xyz = [y, x, z]

            # sample
            x_batch[i, :, :, :] = scipy.ndimage.map_coordinates(x_batch[i, :, :, :], new_xyz, order=0)
            y_batch[i, :, :, :] = scipy.ndimage.map_coordinates(y_batch[i, :, :, :], new_xyz, order=0)

            # add noise to x_batch  # TODO
            noise = np.random.normal(loc=0.0, scale=0.02*np.std(x_batch[i, :, :, :]), size=x_batch[i, :, :, :].shape)
            x_batch[i, :, :, :] += noise

        return x_batch, y_batch

    @ staticmethod
    def _b0_bias_augmentation_ops(volume):
        # double linear (bi-linear) bias
        volume = add_linear_bias(x_batch=np.copy(volume), cval=[0.85, 1.15])
        volume = add_linear_bias(x_batch=np.copy(volume), cval=[0.85, 1.15])
        return volume

    @ staticmethod
    def stack_some_slices(array, mask_array, num_slices):
        dimens = array.shape

        # start to stack on the first index some randomly chosen slices from one projection:
        tmp = np.transpose(array, (3, 1, 2, 0))
        m_tmp = np.transpose(mask_array, (3, 1, 2, 0))
        idx = np.random.choice(np.arange(len(tmp)), num_slices)
        array_pad = np.array([tmp[x, ...] for x in idx])
        mask_array_pad = np.array([m_tmp[x, ...] for x in idx])

        # now add the other projections:
        tmp = np.transpose(array, (2, 1, 3, 0))
        m_tmp = np.transpose(mask_array, (2, 1, 3, 0))
        idx = np.random.choice(np.arange(len(tmp)), num_slices)
        _to_append = np_zero_pad(
            np.array([tmp[x, ...] for x in idx]),
            size=(num_slices, dimens[1], dimens[2], 1), offset=[0, 0, (dimens[1] - dimens[3]) // 2, 0])
        _to_append_mask = np_zero_pad(
            np.array([m_tmp[x, ...] for x in idx]),
            size=(num_slices, dimens[1], dimens[2], 1), offset=[0, 0, (dimens[1] - dimens[3]) // 2, 0])
        array_pad = np.append(array_pad, _to_append, axis=0)
        mask_array_pad = np.append(mask_array_pad, _to_append_mask, axis=0)

        tmp = np.transpose(array, (1, 2, 3, 0))
        m_tmp = np.transpose(mask_array, (1, 2, 3, 0))
        idx = np.random.choice(np.arange(len(tmp)), num_slices)
        _to_append = np_zero_pad(
            np.array([tmp[x, ...] for x in idx]),
            size=(num_slices, dimens[1], dimens[2], 1), offset=[0, 0, (dimens[1] - dimens[3]) // 2, 0])
        _to_append_mask = np_zero_pad(
            np.array([m_tmp[x, ...] for x in idx]),
            size=(num_slices, dimens[1], dimens[2], 1), offset=[0, 0, (dimens[1] - dimens[3]) // 2, 0])
        array_pad = np.append(array_pad, _to_append, axis=0)
        mask_array_pad = np.append(mask_array_pad, _to_append_mask, axis=0)

        return array_pad, mask_array_pad

    def _parse_nifti_data(self, path, augment=False, standardize=False):
        """ python function to parse nifti data to tf.data.Dataset object.
            augment: if True apply data augmentation
        """
        path_str = path.decode('utf-8')
        fname = path_str.split('.')[0]
        ext = '.nii.gz'
        volume = nib.load(fname + ext).get_data().astype(np.float32)
        mask = nib.load(fname + '_brainmask' + ext).get_data().astype(np.float32)

        # undersampling:  # TODO: this is due to memory constraints
        volume = block_reduce(volume, block_size=(2, 2, 2), func=np.mean)
        mask = block_reduce(mask, block_size=(2, 2, 2), func=np.mean)
        mask = np.round(mask)

        dimens = volume.shape
        assert dimens == mask.shape

        # volume = volume.transpose((2, 0, 1))
        # mask = mask.transpose((2, 0, 1))
        volume = volume.reshape([-1, dimens[0], dimens[1], dimens[2]])
        mask = mask.reshape([-1, dimens[0], dimens[1], dimens[2]])

        if augment:  # data augmentation
            volume, mask = self._data_augmentation_ops(volume, mask)
            volume = self._b0_bias_augmentation_ops(volume)

        volume, mask = self.stack_some_slices(volume, mask, num_slices=32)
        mask = self.one_hot_encode(mask, nb_classes=2).astype(np.bool)

        if standardize:
            volume = volume.astype(np.float32)
            volume -= np.mean(volume)
            volume /= np.std(volume)

        return volume, mask

    def get_data(self, list_of_files_train, list_of_files_valid, b_size, augment=False, standardize=False,
                 num_threads=4):
        """ Returns iterators on the dataset along with their initializers.
        :param list_of_files_train: list of strings, path for the train_set files
        :param list_of_files_valid: list of strings, path for the validation_set files
        :param b_size: batch size
        :param augment: if to perform data augmentation
        :param standardize: if to standardize the input data
        :param num_threads: for parallelization
        :return: train_init, valid_init, input_data, label
        """
        with tf.name_scope('data'):
            train_filenames = tf.constant(list_of_files_train)
            valid_filenames = tf.constant(list_of_files_valid)

            train_data = tf.data.Dataset.from_tensor_slices(train_filenames)
            train_data = train_data.map(lambda filename: tf.py_func(  # Parse the record into tensors  # provare Dataset.from_generator
                self._parse_nifti_data,
                [filename, augment, standardize],
                [tf.float32, tf.bool]), num_parallel_calls=num_threads)

            valid_data = tf.data.Dataset.from_tensor_slices(valid_filenames)
            valid_data = valid_data.map(lambda filename: tf.py_func(  # Parse the record into tensors
                self._parse_nifti_data,
                [filename, False, standardize],
                [tf.float32, tf.bool]), num_parallel_calls=num_threads)

            train_data = train_data.shuffle(buffer_size=len(list_of_files_train))
            # train_data = train_data.repeat()  # Repeat the input indefinitely

            train_data = train_data.batch(b_size)

            if len(get_available_gpus()) > 0:
                if tf.__version__ < '1.7.0':
                    train_data = train_data.prefetch(buffer_size=2)  # buffer_size dipende dal pc
                else:
                    # train_data = train_data.apply(tf.contrib.data.prefetch_to_device("/gpu:0"))  # sceglie automaticamente il buffer_size (tf>=1.7)
                    train_data = train_data.apply(prefetching_ops.copy_to_device("/gpu:0")).prefetch(2)  # sceglie automaticamente il buffer_size (tf>=1.7)

            iterator = tf.data.Iterator.from_structure(train_data.output_types, train_data.output_shapes)

            input_data, output_data = iterator.get_next()
            train_init = iterator.make_initializer(train_data)  # initializer for train_data
            valid_init = iterator.make_initializer(valid_data)  # initializer for test_data

            with tf.name_scope('input'):
                input_data = tf.reshape(input_data, shape=[-1, 128, 128, 1])

            with tf.name_scope('output'):
                output_data = tf.cast(tf.reshape(output_data, shape=[-1, 128, 128, 2]), tf.int8)

            return train_init, valid_init, input_data, output_data

    def get_data_v2(self, list_of_files_train, list_of_files_valid, b_size, augment=False, standardize=False,
                    num_threads=4):
        # TODO: questa va testata  << ----------------------------
        # TODO: questa va testata  << ----------------------------
        with tf.name_scope('data'):
            train_filenames = tf.constant(list_of_files_train)
            valid_filenames = tf.constant(list_of_files_valid)

            train_data = tf.data.Dataset.from_tensor_slices(train_filenames)

            train_data = train_data.apply(tf.contrib.data.shuffle_and_repeat(buffer_size=len(list_of_files_train)))
            train_data = train_data.apply(
                tf.contrib.data.map_and_batch(lambda filename: tf.py_func(  # provare Dataset.from_generator
                    self._parse_nifti_data,
                    [filename, standardize, augment],
                    [tf.float32, tf.bool]), batch_size=b_size, num_parallel_batches=1))

            valid_data = tf.data.Dataset.from_tensor_slices(valid_filenames)
            valid_data = valid_data.map(lambda filename: tf.py_func(
                self._parse_nifti_data,
                [filename, standardize, False],
                [tf.float32, tf.bool]), num_parallel_calls=num_threads)

            train_data.prefetch(buffer_size=2)  # il numero dipende dalle prestazioni complessive

            iterator = tf.data.Iterator.from_structure(train_data.output_types, train_data.output_shapes)
            input_data, label = iterator.get_next()
            train_init = iterator.make_initializer(train_data)  # initializer for train_data
            test_init = iterator.make_initializer(valid_data)  # initializer for train_data

            input_data = tf.reshape(input_data, shape=[-1, 256, 256, 1])
            label = tf.cast(tf.reshape(label, shape=[-1, 256, 256, 2]), tf.int8)

            return (train_init, test_init), (input_data, label)

