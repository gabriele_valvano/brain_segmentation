""" Test network (unet 2d)
The output mask is saved in the input folder; moreover, many metrics are evaluated for a final report.
These metrics are: 'processing time (s)', 'dice coefficient', 'FNR', 'FPR'
"""
#  Copyright 2019 Gabriele Valvano
# 
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import tensorflow as tf
from unet_2d.unet import ConvNet
import os
from time import strftime
import errno
from skimage.transform import resize
from skimage.measure import block_reduce
from idas.data_utils.nifti_utils import *
from idas.metrics import py_metrics
import numpy as np
import time
from idas.data_augmentation.numpy_utils import np_zero_pad
from scipy.ndimage.measurements import label


volume_data_root = 'data/NFBS_Dataset'

CLEAN_FINAL_MASK = True


# ________________________________________________ #


def get_volume_path(root):
    # returns list of NFBS dataset brain files under root
    list_path = []
    for dirName, subdirList, fileList in os.walk(root):
        subdirList.sort()
        for subdir in subdirList:
            if not subdir.startswith('.'):
                list_path.append(root + os.sep + subdir + os.sep + 'sub-' + subdir + '_ses-NFB3_T1w.nii.gz')
    return list_path


def get_data(file_name):
    mask_name = file_name.split('.')[0] + '_brainmask.nii.gz'

    vol, vol_affine = get_nifti_matrix(file_name, dtype=np.float32)
    msk, msk_affine = get_nifti_matrix(mask_name, dtype=np.float32)

    vol = block_reduce(vol, block_size=(2, 2, 2), func=np.mean)
    msk = block_reduce(msk, block_size=(2, 2, 2), func=np.mean)
    msk = np.round(msk)

    dims = vol.shape
    assert dims == msk.shape

    vol = vol.reshape([dims[0], dims[1], dims[2], 1])
    msk = msk.reshape([dims[0], dims[1], dims[2], 1])
    return vol, msk, vol_affine, msk_affine


def compute_weighted_mask(mask_list, eps=1e-12):

    # only three projections:
    assert len(mask_list) == 3

    dims = mask_list[0].shape

    w_vec1 = np.sum(mask_list[0], axis=(1, 2), keepdims=True)
    w_mat1 = np.tile(w_vec1, (1, dims[1], dims[2]))

    w_vec2 = np.sum(mask_list[1], axis=(0, 2), keepdims=True)
    w_mat2 = np.tile(w_vec2, (dims[0], 1, dims[2]))

    w_vec3 = np.sum(mask_list[2], axis=(0, 1), keepdims=True)
    w_mat3 = np.tile(w_vec3, (dims[0], dims[1], 1))

    w_mat_sum = 1. * (w_mat1 + w_mat2 + w_mat3 + eps)

    weighted_mask = (w_mat1 * 1. / w_mat_sum) * mask_list[0] + \
                    (w_mat2 * 1. / w_mat_sum) * mask_list[1] + \
                    (w_mat3 * 1. / w_mat_sum) * mask_list[2]

    return weighted_mask


def remove_spurious_ones(mmask):
    s = np.ones((3, 3, 3))
    labeled_array, num_features = label(mmask, structure=s)
    unique, counts = np.unique(labeled_array, return_counts=True)
    d = dict(zip(unique, counts))
    del d[0]
    mkey = 0
    mvalue = 0
    for (k, v) in d.items():
        if v >= mvalue:
            mvalue = v
            mkey = k
    return (labeled_array == mkey).astype(np.int8)


def report_results(res_dict):
    print('\n' + '-'*10)
    for key, value in zip(res_dict.keys(), res_dict.values()):
        print('\033[33m\nMetric: {0} = \033[0m{1:.5f}'.format(key, value))
    print('\n' + '-'*10 + '\n')


if __name__ == '__main___':

    print('\n' + '-'*10)
    print('\033[33m\nStarting test at {0}\033[0m'.format(strftime("%H:%M:%S")))

    list_of_files_test = get_volume_path(volume_data_root + os.sep + 'test_set')

    model = ConvNet()
    model.build()

    # lists of all the metrics I want to evaluate
    time_list = []
    dice_list = []
    FNR_list = []
    FPR_list = []

    print('\nEvaluating the prediction performing tf.argmax() on the last index.')
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        saver = tf.train.Saver()
        ckpt = tf.train.get_checkpoint_state(os.path.dirname(model.checkpoint_dir + '/checkpoint'))
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
        else:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT),
                                    model.checkpoint_dir + ' (checkpoint_dir)')

        cont = 0
        for fname in list_of_files_test:
            print('file # ', cont, ' out of ', len(list_of_files_test))
            cont += 1

            out_file = fname.split('.')[0] + '_unet2dmask' + '.nii.gz'
            volume, mask, _, mask_affine = get_data(fname)

            # _________________________________
            # saving undersampled version
            save_nifti_matrix(np.squeeze(volume), mask_affine, fname.split('.')[0] + '_input_low_res' + '.nii.gz')

            if model.stdz:
                volume = volume.astype(np.float32)
                volume -= np.mean(volume)
                volume /= np.std(volume)

            start_time = time.time()
            list_outputs = []
            delta = (128 - 96) // 2
            for axis in [0, 1, 2]:
                if axis == 0:
                    _volume = np_zero_pad(volume, size=(128, 128, 128, 1), offset=[0, 0, delta, 0])  # delta, 0])
                elif axis == 1:
                    _volume = np.transpose(volume, (1, 2, 0, 3))
                    _volume = np_zero_pad(_volume, size=(128, 128, 128, 1), offset=[0, delta, 0, 0])  # delta, 0])
                elif axis == 2:
                    _volume = np.transpose(volume, (0, 2, 1, 3))
                    _volume = np_zero_pad(_volume, size=(128, 128, 128, 1), offset=[0, delta, 0, 0])  # delta, 0])
                else:
                    break

                eval_mask = sess.run(tf.argmax(model.prediction, -1),
                                     feed_dict={model.input_data: _volume, model.is_training: False})

                eval_mask = eval_mask.reshape(-1, 128, 128, 1)

                if axis == 0:
                    eval_mask = eval_mask[:, :, delta:-delta, :]
                elif axis == 1:
                    eval_mask = np.transpose(eval_mask, (2, 0, 1, 3))
                    eval_mask = eval_mask[:, :, delta:-delta, :]
                elif axis == 2:
                    eval_mask = np.transpose(eval_mask, (0, 2, 1, 3))
                    eval_mask = eval_mask[:, :, delta:-delta, :]

                list_outputs.append(np.squeeze(eval_mask))

            output = compute_weighted_mask(list_outputs).astype(bool)

            if CLEAN_FINAL_MASK:
                output = remove_spurious_ones(np.squeeze(output))

            # _________________________________
            # compute metrics:
            # time
            delta_t = time.time() - start_time
            time_list.append(delta_t)

            output = np.squeeze(output)
            mask = np.squeeze(mask)

            # dice
            dice = py_metrics.eval_dice(seg=output, gt=mask)
            dice_list.append(dice)

            # FNR and FPR
            tnr, fpr, fnr, tpr = py_metrics.true_false_positives_negatives(true=mask, pred=output, normalize=True)
            FNR_list.append(fnr)
            FPR_list.append(fpr)

            # _________________________________
            # saving undersampled version
            save_nifti_matrix(output, mask_affine, fname.split('.')[0] + '_unet2dmask_low_res' + '.nii.gz')

            # _________________________________
            # save output mask
            ups_output = resize(output, output_shape=[256, 256, 192], order=0, preserve_range=True)
            save_nifti_matrix(ups_output, mask_affine, out_file)

    print('REPORT RUN_ID = {0}'.format(model.run_id))
    rdict = dict()
    rdict['time: mean'] = np.mean(time_list)
    rdict['time: std'] = np.std(time_list)
    rdict['dice: mean'] = np.mean(dice_list)
    rdict['dice: std'] = np.std(dice_list)
    rdict['FNR: mean'] = np.mean(FNR_list)
    rdict['FNR: std'] = np.std(FNR_list)
    rdict['FPR: mean'] = np.mean(FPR_list)
    rdict['FPR: std'] = np.std(FPR_list)
    report_results(res_dict=rdict)

    print('\033[33m\nEnd of the test procedure at {0}\033[0m'.format(strftime("%H:%M:%S")))


# ---------------------------------
# CLEAN_FINAL_MASK = False
#
# Metric: time: mean = 2.40765
# Metric: time: std = 0.16938
#
# Metric: dice: mean = 0.97120
# Metric: dice: std = 0.00651
#
# Metric: FNR: mean = 0.00294
# Metric: FNR: std = 0.00209
#
# Metric: FPR: mean = 0.00292
# Metric: FPR: std = 0.00136
#
# ---------------------------------
# ---------------------------------
# CLEAN_FINAL_MASK = True
#
# Metric: time: mean = 2.42495
# Metric: time: std = 0.16925
#
# Metric: dice: mean = 0.97255
# Metric: dice: std = 0.00582
#
# Metric: FNR: mean = 0.00294
# Metric: FNR: std = 0.00209
#
# Metric: FPR: mean = 0.00265
# Metric: FPR: std = 0.00143
#
# ---------------------------------
