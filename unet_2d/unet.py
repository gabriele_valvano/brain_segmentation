"""
Unet 2d
"""
#  Copyright 2019 Gabriele Valvano
# 
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import time
from idas import utils
import tensorflow as tf
from unet_2d.data_set_interface import DatasetInterface
import errno
from idas.callbacks import callbacks as tf_callbacks
from idas.callbacks.routine_callback import RoutineCallback
from tensorflow.layers import conv2d, max_pooling2d, batch_normalization, conv2d_transpose
import numpy as np

# _______________________________________________________________________________________________________________ #

from unet_2d import config_file
FLAGS = config_file.define_flags()

# _______________________________________________________________________________________________________________ #


def _encode_brick(incoming, nb_filters, is_training, scope, trainable=True):
    k_init = tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_IN', uniform=False)  # He init.
    b_init = tf.zeros_initializer()
    with tf.variable_scope(scope):
        conv1 = conv2d(incoming, filters=nb_filters, kernel_size=3, strides=1, padding='same',
                       kernel_initializer=k_init, bias_initializer=b_init, trainable=trainable)
        conv1_act = tf.nn.relu(conv1)
        conv1_bn = batch_normalization(conv1_act, training=is_training, trainable=trainable)

        conv2 = conv2d(conv1_bn, filters=nb_filters, kernel_size=3, strides=1, padding='same',
                       kernel_initializer=k_init, bias_initializer=b_init, trainable=trainable)
        conv2_act = tf.nn.relu(conv2)
        conv2_bn = batch_normalization(conv2_act, training=is_training, trainable=trainable)

        pool = max_pooling2d(conv2_bn, pool_size=2, strides=2, padding='same')

        with tf.variable_scope('concat_layer_out'):
            concat_layer_out = conv2_bn
    return pool, concat_layer_out


def _decode_brick(incoming, concat_layer_in, nb_filters, is_training, scope):
    k_init = tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_IN', uniform=False)  # He init.
    b_init = tf.zeros_initializer()
    with tf.variable_scope(scope):
        conv1t = conv2d_transpose(incoming, filters=nb_filters, kernel_size=2, strides=2, padding='same',
                                  kernel_initializer=k_init, bias_initializer=b_init)
        conv1t_act = tf.nn.relu(conv1t)
        conv1t_bn = batch_normalization(conv1t_act, training=is_training)

        concat = tf.concat([conv1t_bn, concat_layer_in], axis=-1)

        conv2 = conv2d(concat, filters=nb_filters, kernel_size=3, strides=1, padding='same', kernel_initializer=k_init,
                       bias_initializer=b_init)
        conv2_act = tf.nn.relu(conv2)
        conv2_bn = batch_normalization(conv2_act, training=is_training)

        conv3 = conv2d(conv2_bn, filters=nb_filters, kernel_size=3, strides=1, padding='same', kernel_initializer=k_init,
                       bias_initializer=b_init)
        conv3_act = tf.nn.relu(conv3)
        conv3_bn = batch_normalization(conv3_act, training=is_training)

    return conv3_bn


def _ouput_layer(incoming, nb_classes, scope):
    k_init = tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_IN', uniform=False)  # He init.
    b_init = tf.zeros_initializer()

    with tf.variable_scope(scope):
        output = conv2d(incoming, filters=nb_classes, kernel_size=1, strides=1, padding='same',
                        kernel_initializer=k_init, bias_initializer=b_init)
        # activation = linear
    return output

# _______________________________________________________________________________________________________________ #


class ConvNet(DatasetInterface):
    def __init__(self, run_id=None):
        super().__init__()

        # check weather to take a specific RUN_ID
        if run_id is not None:
            FLAGS.RUN_ID = run_id
        self.run_id = FLAGS.RUN_ID

        self.lr = tf.Variable(FLAGS.lr, dtype=tf.float32, trainable=False, name='learning_rate')  # learning rate
        self.batch_size = FLAGS.b_size
        self.global_epoch = 0

        # path to save checkpoints and graph
        self.checkpoint_dir = '~/../../DATA2/results/checkpoints/' + FLAGS.RUN_ID
        self.graph_dir = '~/../../DATA2/results/graphs/' + FLAGS.RUN_ID + '/convnet'
        self.history_log_dir = '~/../../DATA2/results/history_logs/' + FLAGS.RUN_ID

        # list of path for the training and validation files:
        self.list_of_files_train = FLAGS.list_of_files_train
        self.list_of_files_valid = FLAGS.list_of_files_valid

        # init the list of callbacks to be called and relative arguments
        self.callbacks = []
        self.callbacks_kwargs = {'history_log_dir': self.history_log_dir}

        # routine callback always runs:
        self.callbacks.append(RoutineCallback())

        # Define global step for training e validation and counter for global epoch:
        self.g_train_step = tf.Variable(0, dtype=tf.int32, trainable=False, name='global_train_step')
        self.g_valid_step = tf.Variable(0, dtype=tf.int32, trainable=False, name='global_validation_step')
        self.g_epoch = tf.Variable(0, dtype=tf.int32, trainable=False, name='global_epoch')

        # other variables:
        self.is_training = tf.placeholder(dtype=tf.bool, name='is_training')  # training or test mode (needed for the behaviour of dropout, BN, ecc.)
        self.skip_step = FLAGS.skip_step  # frequency of batch report
        self.tensorboard_verbose = FLAGS.tensorboard_verbose  # if True: save also layers weights at the end of epoch

    def get_data(self, num_threads, *kwargs):
        self.augm = True  # perform data augmentation
        self.stdz = True  # perform data standardization
        print("Data augmentation: \033[94m{0}\033[0m, Data standardization: \033[94m{1}\033[0m.".format(self.augm,
                                                                                                        self.stdz))
        self.train_init, self.valid_init, self.input_data, self.output_data = \
            super(ConvNet, self).get_data(self.list_of_files_train, self.list_of_files_valid, b_size=self.batch_size,
                                          augment=self.augm, standardize=self.stdz, num_threads=num_threads)

    def inference(self):

        with tf.variable_scope('U-Net_2D'):

            with tf.variable_scope('Encoder'):
                en_brick_0, concat_0 = _encode_brick(self.input_data, 32, is_training=self.is_training, scope='encode_brick_0')
                en_brick_1, concat_1 = _encode_brick(en_brick_0, 64, is_training=self.is_training, scope='encode_brick_1')
                en_brick_2, concat_2 = _encode_brick(en_brick_1, 128, is_training=self.is_training, scope='encode_brick_2')
                en_brick_3, concat_3 = _encode_brick(en_brick_2, 256, is_training=self.is_training, scope='encode_brick_3')
                en_brick_4, concat_4 = _encode_brick(en_brick_3, 256, is_training=self.is_training, scope='encode_brick_4')

            with tf.variable_scope('Decoder'):
                dec_brick_0 = _decode_brick(en_brick_4, concat_layer_in=concat_4, nb_filters=256, is_training=self.is_training, scope='decode_brick_0')
                dec_brick_1 = _decode_brick(dec_brick_0, concat_layer_in=concat_3, nb_filters=256, is_training=self.is_training, scope='decode_brick_1')
                dec_brick_2 = _decode_brick(dec_brick_1, concat_layer_in=concat_2, nb_filters=128, is_training=self.is_training, scope='decode_brick_2')
                dec_brick_3 = _decode_brick(dec_brick_2, concat_layer_in=concat_1, nb_filters=64, is_training=self.is_training, scope='decode_brick_3')
                dec_brick_4 = _decode_brick(dec_brick_3, concat_layer_in=concat_0, nb_filters=32, is_training=self.is_training, scope='decode_brick_4')

            # Output
            output = _ouput_layer(dec_brick_4, nb_classes=2, scope='ouput_layer')

            self.prediction = output

    def loss(self):
        with tf.variable_scope('Weighted_cross_entropy'):
            n_voxels = 128 * 128 * 96 * 1
            # weights = np.array([0.085, 0.915])

            a = tf.divide(1., tf.reduce_sum(tf.cast(self.output_data, tf.float32)))
            b = tf.divide(1., (n_voxels - a))
            weights = [b, a]  # [1/(number of zeros), 1/(number of ones)]

            epsilon = tf.constant(1e-12, dtype=tf.float32)
            print("Loss function: \033[94m{0}\033[0m".format('weighted cross_entropy'))

            num_classes = self.prediction.get_shape().as_list()[-1]  # class on the last index
            assert (num_classes is not None)
            assert len(weights) == num_classes

            y_pred = tf.reshape(self.prediction, (-1, num_classes))
            y_true = tf.to_float(tf.reshape(self.output_data, (-1, num_classes)))
            softmax = tf.nn.softmax(y_pred) + epsilon

            w_cross_entropy = -tf.reduce_sum(tf.multiply(y_true * tf.log(softmax), weights), reduction_indices=[1])
            self.loss_unet = tf.reduce_mean(w_cross_entropy, name='weighted_cross_entropy')

        self.loss = self.loss_unet

    def optimize(self):
        """
        Define training op
        using Adam Gradient Descent to minimize cost
        """
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)  # to update batch normalization population statistics
        with tf.control_dependencies(update_ops):
            self.train_op = tf.train.AdamOptimizer(self.lr).minimize(self.loss, global_step=self.g_train_step)

    def summary(self):
        """
        Create summaries to write on TensorBoard
        """
        with tf.name_scope('Loss'):
            l_tr = tf.summary.scalar('train_set', self.loss)
            l_val = tf.summary.scalar('validation_set', self.loss)
        with tf.name_scope('Accuracy'):
            a_tr = tf.summary.scalar('train_set', self.accuracy)
            a_val = tf.summary.scalar('validation_set', self.accuracy)
        with tf.name_scope('Dice'):
            d_tr = tf.summary.scalar('train_set', self.dice_metric)
            d_val = tf.summary.scalar('validation_set', self.dice_metric)

        train_summaries = [a_tr, d_tr, l_tr]
        valid_summaries = [a_val, d_val, l_val]

        if self.perform_lr_annealing:
            with tf.name_scope('LearningRate'):
                train_summaries.append(tf.summary.scalar('train_set', self.lr))
                valid_summaries.append(tf.summary.scalar('validation_set', self.lr))

        self.train_summary_op = tf.summary.merge(train_summaries)
        self.valid_summary_op = tf.summary.merge(valid_summaries)

        # ---- #
        if self.tensorboard_verbose:
            vars = [v.name for v in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if ('kernel' in v.name)]
            weights_summary = [tf.summary.histogram(v, tf.get_default_graph().get_tensor_by_name(v)) for v in vars]
            self.weights_summary = tf.summary.merge(weights_summary)

    def eval(self):
        """
        Count the number of right predictions in a batch
        """
        with tf.name_scope('predict'):
            # Accuracy metric:
            preds = tf.nn.softmax(self.prediction)
            correct_preds = tf.equal(tf.argmax(preds, -1), tf.argmax(self.output_data, -1))
            self.accuracy = tf.reduce_mean(tf.cast(correct_preds, tf.float32))

            # Dice metric:
            true_positives = tf.logical_and(tf.equal(tf.argmax(self.output_data, -1), True), tf.equal(tf.argmax(preds, -1), True))
            false_positives = tf.logical_and(tf.equal(tf.argmax(self.output_data, -1), False), tf.equal(tf.argmax(preds, -1), True))
            false_negatives = tf.logical_and(tf.equal(tf.argmax(self.output_data, -1), True), tf.equal(tf.argmax(preds, -1), False))
            true_positives_sum = tf.reduce_sum(tf.to_float(true_positives))
            false_positives_sum = tf.reduce_sum(tf.to_float(false_positives))
            false_negatives_sum = tf.reduce_sum(tf.to_float(false_negatives))
            eps = tf.constant(1e-16)
            self.dice_metric = (2.*true_positives_sum + eps) / (2.*true_positives_sum + false_positives_sum + false_negatives_sum + eps)

    def build(self):
        """ Build the computation graph """
        print('Building the computation graph...\nRUN_ID = \033[94m{0}\033[0m'.format(FLAGS.RUN_ID))
        self.get_data(num_threads=FLAGS.num_threads)
        self.inference()
        self.loss()
        self.optimize()
        self.eval()
        self.summary()

    def train_one_epoch(self, sess, init, writer, step, caller):
        """ train the model for one epoch. """
        start_time = time.time()
        sess.run(init)
        total_loss = 0
        total_correct_preds = 0
        n_batches = 0
        try:
            while True:
                caller.on_batch_begin(training_state=True, **self.callbacks_kwargs)

                _, l, a, summaries = sess.run([self.train_op, self.loss, self.accuracy, self.train_summary_op],
                                              feed_dict={self.is_training: True})  # 'is_training:0':)
                writer.add_summary(summaries, global_step=step)
                step += 1
                total_loss += l
                total_correct_preds += a
                n_batches += 1
                if (n_batches % self.skip_step) == 0:
                    print('\r  ...training over batch {1}: {0} batch_loss = {2:.4f} {0} batch_accuracy = {3:.4f}'
                          .format(' '*3, n_batches, l, a), end='\n')

                caller.on_batch_end(training_state=True, **self.callbacks_kwargs)
        except tf.errors.OutOfRangeError:
            # Fine dell'epoca. Qui valutare eventuali statistiche, fare log, ecc..
            avg_loss = total_loss/n_batches
            avg_acc = total_correct_preds/n_batches
            delta_t = time.time() - start_time
            pass

        # update global epoch counter:
        sess.run(self.g_epoch.assign_add(1))

        print('\033[31m  TRAIN\033[0m:{0}{0} average loss = {1:.4f} {0} average accuracy = {2:.4f} {0} Took: {3:.3f} seconds'
              .format(' '*3, avg_loss, avg_acc, delta_t))
        return step

    def eval_layer_activation(self, input_data, layer_name):
        """ Test the model on input_data
        layer_name = name_scope of the layer to evaluate
        i.e. layer_name = 'network/layer_conv1/Relu:0'  (notice ':0' at the end)
        """
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            saver = tf.train.Saver()
            ckpt = tf.train.get_checkpoint_state(os.path.dirname(self.checkpoint_dir + '/checkpoint'))
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)
                layer = tf.get_default_graph().get_tensor_by_name(layer_name)
                output = sess.run(layer, feed_dict={self.input_data: input_data, self.is_training: False})
                return output
            else:
                raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), self.checkpoint_dir +
                                        ' (checkpoint_dir)')

    def eval_once(self, sess, init, writer, step, caller):
        """ Eval the model once """
        start_time = time.time()
        sess.run(init)
        total_correct_preds = 0
        total_loss = 0
        n_batches = 0
        try:
            while True:
                caller.on_batch_begin(training_state=False, **self.callbacks_kwargs)

                loss_batch, accuracy_batch, summaries = sess.run([self.loss, self.accuracy, self.valid_summary_op],
                                                                 feed_dict={self.is_training: False})
                writer.add_summary(summaries, global_step=step)
                step += 1
                total_loss += loss_batch
                total_correct_preds += accuracy_batch
                n_batches += 1

                caller.on_batch_end(training_state=False, **self.callbacks_kwargs)
        except tf.errors.OutOfRangeError:
            # Fine del validation_set set. Qui valutare eventuali statistiche, fare log, ecc..
            avg_loss = total_loss/n_batches
            avg_acc = total_correct_preds/n_batches
            delta_t = time.time() - start_time
            pass

        # update global epoch counter:
        sess.run(self.g_valid_step.assign(step))

        print('\033[31m  VALIDATION\033[0m:  average loss = {1:.4f} {0} average accuracy = {2:.4f} {0} Took: {3:.3f} seconds'
              .format(' '*3, avg_loss, avg_acc, delta_t))
        return step

    def test(self, input_data):
        """ Test the model on input_data """

        if self.stdz:
            mu, sigma = np.mean(input_data), np.std(input_data)
            input_data = (input_data - mu) / sigma

        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())
            saver = tf.train.Saver()
            ckpt = tf.train.get_checkpoint_state(os.path.dirname(self.checkpoint_dir + '/checkpoint'))
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)
                print('Evaluating the prediction performing tf.argmax() on the last index.')
                output = sess.run(tf.argmax(self.prediction, -1),
                                  feed_dict={self.input_data: input_data, self.is_training: False})
                return output
            else:
                raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT),
                                        self.checkpoint_dir + ' (checkpoint_dir)')

    def train(self, n_epochs):
        """ The train function alternates between training one epoch and evaluating """
        print("\nStarting network training... Number of epochs to train: \033[94m{0}\033[0m".format(n_epochs))
        print("Tensorboard verbose mode: \033[94m{0}\033[0m".format(self.tensorboard_verbose))
        print("Tensorboard dir: \033[94m{0}\033[0m".format(self.graph_dir))
        utils.safe_mkdir(self.checkpoint_dir)
        utils.safe_mkdir(self.history_log_dir)
        writer = tf.summary.FileWriter(self.graph_dir, tf.get_default_graph())

        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())

            saver = tf.train.Saver()  # keep_checkpoint_every_n_hours=2
            ckpt = tf.train.get_checkpoint_state(os.path.dirname(self.checkpoint_dir + '/checkpoint'))
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)

            print("Model already trained for \033[94m{0}\033[0m epochs.".format(self.g_epoch.eval()))
            t_step = self.g_train_step.eval()  # global step for train
            v_step = self.g_valid_step.eval()  # global step for validation

            # Define a caller to call the callbacks
            self.callbacks_kwargs.update({'sess': sess, 'cnn': self})
            caller = tf_callbacks.ChainCallback(callbacks=self.callbacks)
            caller.on_train_begin(training_state=True, **self.callbacks_kwargs)

            for epoch in range(n_epochs):
                print('_'*40 + '\n\033[1;33mEPOCH {0}:\033[0m'.format(epoch))
                caller.on_epoch_begin(training_state=True, **self.callbacks_kwargs)

                t_step = self.train_one_epoch(sess, self.train_init, writer, t_step, caller)
                caller.on_epoch_end(training_state=True, **self.callbacks_kwargs)

                v_step = self.eval_once(sess, self.valid_init, writer, v_step, caller)

                # save updated variables and weights
                saver.save(sess, self.checkpoint_dir + '/checkpoint', t_step)

                if self.tensorboard_verbose and (epoch % 50 == 0):
                    # writing summary for the weights:
                    summary = sess.run(self.weights_summary)
                    writer.add_summary(summary, global_step=t_step)

            caller.on_train_end(training_state=True, **self.callbacks_kwargs)
        writer.close()


if __name__ == '__main__':
    print('\n' + '-'*3)
    model = ConvNet()
    model.build()
    model.train(n_epochs=2)
